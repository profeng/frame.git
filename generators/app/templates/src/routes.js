const Home = resolve => require(['./views/home.vue'], resolve);
const PageNotFound = resolve => require(['./views/404.vue'], resolve);

// 路由集合
const routes = [
    { name: 'home', path: '/', component: Home, meta: {title: '首页'} },
    { path: '*', meta: {title: '出错啦'}, component: PageNotFound }
];
// 导出路由集合
export default routes;
