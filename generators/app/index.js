'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
// 文件读写模块.
const fs = require('fs');
// 路径模块
const path = require('path');
// 命令行
const {execSync} = require('child_process');

/**
 * vue脚手架
 * 可定制项目名称、创建者、版本号、以及开发环境（dev-server）的端口号；
 */

module.exports = class extends Generator {
    /** 构造函数 */
    constructor (args, opts) {
        super(args, opts);
        this.appName = path.basename(process.cwd());
        this.appAuthor = 'xufeng';
    }

    /** ================== 私有方法 start ================== */
    _copyFolder (tar, dis) {
        if (!tar) return;
        if (!dis) dis = tar;
        if (fs.existsSync(dis)) this._deleteFolder(dis); // 清除已有数据
        this.fs.copy(tar, dis); //
    }
    _deleteFolder (path) { // 删除文件夹及文件
        if (!path) return;
        var files = [];
        if (fs.existsSync(path)) {
            files = fs.readdirSync(path);
            files.forEach((file,index) => {
                var curPath = path + '/' + file;
                if (fs.statSync(curPath).isDirectory()) { // recurse
                    this._deleteFolder(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    }
    /** ================== 私有方法 end ================== */

    /** 初始化 */
    initializing () {
        // this.log('开始构建...');
    }

    /** 交互参数收集 */
    prompting () {
        // Have Yeoman greet the user.
        this.log(yosay(
            'Welcome to the frame Scaffold ' + chalk.red('generator-frame') + '!'
        ));

        const prompts = [{
            type: 'input',
            name: 'projectName',
            message: '请输入您要新建的项目名称: ',
            default: 'app'
        }, {
            type: 'input',
            name: 'projectDec',
            message: '请输入项目说明: ',
            default: 'Simple project, base on frame'
        }, {
            type: 'input',
            name: 'projectAuthor',
            message: '请输入创建者信息: ',
            default: 'xufeng'
        }, {
            type: 'input',
            name: 'projectPort',
            message: '请设定dev-server端口: ',
            default: '9090'
        }, {
            type: 'confirm',
            name: 'projectVirtualTag',
            message: '是否设定虚路径？'
        }];

        return this.prompt(prompts).then(props => {
            this.log('入参: ' + JSON.stringify(props));
            this.props = props;
            // this.props.projectVirtualPath = '';
            if (props.projectVirtualTag) {
                let sec = [{
                    type: 'input',
                    name: 'projectVirtualPath',
                    message: '请设定虚路径名称：',
                    default: props.projectName
                }];
                return this.prompt(sec).then(res => {
                    this.log('入参：' + JSON.stringify(res));
                    this.props.projectVirtualPath = '/' + res.projectVirtualPath;
                });
            } else {
                this.props.projectVirtualPath = '';
            }
        });
    }

    /** 写入配置: package.json */
    configuring () {
        this.log('创建项目目录：' + this.props.projectName);
        if (fs.existsSync(this.props.projectName)) this._deleteFolder(this.props.projectName);
        fs.mkdirSync(this.props.projectName);
        this.destinationRoot(this.props.projectName); // 将工作路径指向项目目录，后续的依赖安装才可运行~
    }

    /** 从github拉取框架代码 */
    cloneGit () {
        this.log('正在拉取框架代码...');
        this.combPath = '_tmp';
        if (fs.existsSync(this.combPath)) this._deleteFolder(this.combPath);
        execSync('git clone https://gitee.com/profeng/vueDemo.git ' + this.combPath, (err, stdout, stderr) => {
            if (err) {
                console.error(err);
                return;
            }
        });
    }

    /** 文件结构整理 */
    writing () {
        this.log('整理文件结构...');
        let distPath = '';
        // 拷贝框架文件.
        this._copyFolder(this.destinationPath(this.combPath + '/build'), distPath + 'build');
        this._copyFolder(this.destinationPath(this.combPath + '/config'), distPath + 'config');
        this._copyFolder(this.destinationPath(this.combPath + '/routes'), distPath + 'routes');
        // 只选择需要的，去除了多余的views和store文件
        this._copyFolder(this.destinationPath(this.combPath + '/src/components'), distPath + 'src/components');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/font'), distPath + 'src/assets/font');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/logo.png'), distPath + 'src/assets/logo.png');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/default-avatar.png'), distPath + 'src/assets/default-avatar.png');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/download-bg.png'), distPath + 'src/assets/download-bg.png');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/image-empty.png'), distPath + 'src/assets/image-empty.png');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/loading.jpg'), distPath + 'src/assets/loading.jpg');
        this._copyFolder(this.destinationPath(this.combPath + '/src/assets/map-marker.png'), distPath + 'src/assets/map-marker.png');
        this._copyFolder(this.destinationPath(this.combPath + '/src/filters'), distPath + 'src/filters');
        this._copyFolder(this.destinationPath(this.combPath + '/src/js'), distPath + 'src/js');
        this._copyFolder(this.destinationPath(this.combPath + '/src/scss'), distPath + 'src/scss');
        this._copyFolder(this.destinationPath(this.combPath + '/src/store/modules/status.js'), distPath + 'src/store/modules/status.js');
        this._copyFolder(this.destinationPath(this.combPath + '/src/store/types.js'), distPath + 'src/store/types.js');
        this._copyFolder(this.templatePath('src/store/index.js'), distPath + 'src/store/index.js'); // 去掉多余的状态
        this._copyFolder(this.destinationPath(this.combPath + '/src/vendor'), distPath + 'src/vendor');
        this._copyFolder(this.destinationPath(this.combPath + '/src/views/404.vue'), distPath + 'src/views/404.vue');
        this._copyFolder(this.destinationPath(this.combPath + '/src/views/home.vue'), distPath + 'src/views/home.vue');
        this._copyFolder(this.destinationPath(this.combPath + '/src/app.vue'), distPath + 'src/app.vue'); // 获取简化后的
        this._copyFolder(this.destinationPath(this.combPath + '/src/config.js'), distPath + 'src/config.js');
        this._copyFolder(this.destinationPath(this.combPath + '/src/main.js'), distPath + 'src/main.js');
        this._copyFolder(this.destinationPath(this.combPath + '/src/router.js'), distPath + 'src/router.js');
        this._copyFolder(this.templatePath('src/routes.js'), distPath + 'src/routes.js'); // 去掉多余的路由

        this._copyFolder(this.destinationPath(this.combPath + '/static'), distPath + 'static');
        this._copyFolder(this.destinationPath(this.combPath + '/.babelrc'), distPath + '.babelrc');
        this._copyFolder(this.destinationPath(this.combPath + '/.editorconfig'), distPath + '.editorconfig');
        this._copyFolder(this.destinationPath(this.combPath + '/.eslintignore'), distPath + '.eslintignore');
        this._copyFolder(this.destinationPath(this.combPath + '/.eslintrc.js'), distPath + '.eslintrc.js');
        this._copyFolder(this.destinationPath(this.combPath + '/.stylelintrc'), distPath + '.stylelintrc');
        this._copyFolder(this.destinationPath(this.combPath + '/.gitignore'), distPath + '.gitignore');
        this._copyFolder(this.destinationPath(this.combPath + '/favicon.ico'), distPath + 'favicon.ico');
        this._copyFolder(this.destinationPath(this.combPath + '/index.html'), distPath + 'index.html');
        this._copyFolder(this.destinationPath(this.combPath + '/README.md'), distPath + 'README.md');
        this._copyFolder(this.destinationPath(this.combPath + '/.htaccess'), distPath + '.htaccess');

        // 创建 dist 空目录.
        if (fs.existsSync(distPath + 'dist')) this._deleteFolder(distPath + 'dist');
        fs.mkdirSync('dist');

        // 配置package.json
        this.log('配置package.json...');
        let jsonNew = this.fs.readJSON(this.destinationPath(this.combPath + '/package.json'));
        jsonNew.name = this.props.projectName;
        jsonNew.version = '1.0.0';
        jsonNew.description = this.props.projectDec + ' - Generated by generator-frame';
        jsonNew.author = this.props.projectAuthor;
        if (fs.existsSync(distPath + 'package.json')) this._deleteFolder(distPath + 'package.json');
        this.fs.writeJSON(distPath + 'package.json', jsonNew);
    }

    /** 安装依赖 */
    install () {
        // 配置路由虚路径
        this.log('配置路由虚路径...', JSON.stringify(this.props.projectVirtualPath));
        let distPath = '',
            conf = fs.readFileSync(this.destinationPath(this.combPath + '/config/index.js'), 'utf8'),
            core = fs.readFileSync(this.destinationPath(this.combPath + '/src/js/core/core.js'), 'utf8'),
            rotr = fs.readFileSync(this.destinationPath(this.combPath + '/src/router.js'), 'utf8'),
            simp = fs.readFileSync(this.destinationPath(this.combPath + '/build/simple.js'), 'utf8'),
            htac = fs.readFileSync(this.destinationPath(this.combPath + '/.htaccess'), 'utf8');
        if (this.props.projectVirtualPath) {
            conf = conf.replace(new RegExp('app', 'gm'), this.props.projectName);
        } else {
            conf = conf.replace(new RegExp('/app', 'gm'), this.props.projectVirtualPath);
        }
        conf = conf.replace(new RegExp('app', 'gm'), this.props.projectName);
        conf = conf.replace(new RegExp('8080', 'gm'), this.props.projectPort);
        fs.writeFileSync(distPath + 'config/index.js', conf, 'utf8');
        core = core.replace(new RegExp('/app', 'gm'), this.props.projectVirtualPath);
        rotr = rotr.replace(new RegExp('/app', 'gm'), this.props.projectVirtualPath);
        fs.writeFileSync(distPath + 'src/router.js', rotr, 'utf8');
        simp = simp.replace(new RegExp("'app'", 'gm'), "'" + this.props.projectName + "'");
        fs.writeFileSync(distPath + 'build/simple.js', simp, 'utf8');
        htac = htac.replace(new RegExp('/app', 'gm'), this.props.projectVirtualPath);
        fs.writeFileSync(distPath + '.htaccess', htac, 'utf8');

        this.log('安装依赖...');
        // this.installDependencies({ bower: false }); // 安装依赖
    }

    end () {
        this.log('清除框架缓存：' + this.combPath);
        if (fs.existsSync(this.combPath)) this._deleteFolder(this.combPath); // 清除框架代码
        this.log('项目：' + this.props.projectName + '已创建完成！');
        this.log('您可使用：cd ' + this.props.projectName + '&&npm install(cnpm install) && npm run dev 启动项目~');
    }
};
